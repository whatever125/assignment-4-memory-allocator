#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* __restrict addr, block_size block_sz, void* __restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
  if (!addr) {
    return REGION_INVALID;
  }
  size_t region_size = region_actual_size(query + offsetof(struct block_header, contents));
  void* region_address = map_pages(addr, region_size, MAP_FIXED_NOREPLACE);
  if (region_address == MAP_FAILED) {
    region_address = map_pages(addr, region_size, 0);
  }
  if (region_address == MAP_FAILED) {
    return REGION_INVALID;
  }
  block_init(region_address, (block_size) {region_size}, NULL);
  return (struct region) {
    .addr = region_address,
    .size = region_size,
    .extends = (region_address == addr)
  };
}

static void* block_after( struct block_header const* block )         ;
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

/*  освободить всю память, выделенную под кучу */
void heap_term( ) {
  struct block_header* block_addr = HEAP_START;
  void* block_start = block_addr;

  while(block_addr) {
    size_t block_length = 0;
    while (block_addr->next && blocks_continuous(block_addr, block_addr->next)) {
      block_length += size_from_capacity(block_addr->capacity).bytes;
      block_addr = block_addr->next;
    }
    if (block_addr) {
      block_length += size_from_capacity(block_addr->capacity).bytes;
      block_addr = block_addr->next;
    }

    munmap(block_start, block_length);
    block_start = block_addr;
  }
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* __restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  if (!block || !block_splittable(block, query)) {
    return false;
  }
  query = size_max(BLOCK_MIN_CAPACITY, query);
  void* second_address = block->contents + query;
  block_size second_size = (block_size) {block->capacity.bytes - query};
  block_init(second_address, second_size, block->next);

  block->capacity.bytes = query;
  block->next = second_address;
  return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}

static bool mergeable(struct block_header const* __restrict fst, struct block_header const* __restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  if (!(block && block->next && mergeable(block, block->next))) {
    return false;
  }
  size_t new_block_size = size_from_capacity(block->capacity).bytes + size_from_capacity(block->next->capacity).bytes;
  struct block_header* new_next = block->next->next;
  block_init(block, (block_size) {new_block_size}, new_next);
  return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* __restrict block, size_t sz )    {
  if (!block) {
    return (struct block_search_result) {
      .type = BSR_CORRUPTED, 
      .block = NULL
    };
  }
  struct block_header* prev_block = NULL;
  struct block_header* current_block = block;
  while (current_block) {
    while (try_merge_with_next(current_block)) {};
    if (current_block->is_free && current_block->capacity.bytes >= sz) {
      return (struct block_search_result) {
        .type = BSR_FOUND_GOOD_BLOCK,
        .block = current_block
      };
    }
    prev_block = current_block;
    current_block = current_block->next;
  }
  return (struct block_search_result) {
    .type = BSR_REACHED_END_NOT_FOUND,
    .block = prev_block
  };
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  struct block_search_result result = find_good_or_last(block, query);
  if (result.type == BSR_FOUND_GOOD_BLOCK) {
    split_if_too_big(result.block, query);
    result.block->is_free = false;
  }
  return result;
}



static struct block_header* grow_heap( struct block_header* __restrict last, size_t query ) {
  struct region new_region = alloc_region(block_after(last), query);
  if (region_is_invalid(&new_region)) {
    return NULL;
  }
  last->next = new_region.addr;
  if (try_merge_with_next(last)) {
    return last;
  }
  return last->next;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  if (!heap_start) {
    return NULL;
  }
  query = size_max(query, BLOCK_MIN_CAPACITY);
  struct block_search_result result = try_memalloc_existing(query, heap_start);
  if (result.type == BSR_FOUND_GOOD_BLOCK) {
    return result.block;
  }
  if (result.type == BSR_CORRUPTED) {
    return NULL;
  }
  if (result.type == BSR_REACHED_END_NOT_FOUND) {
    result = try_memalloc_existing(query, grow_heap(result.block, query));
    if (result.type == BSR_FOUND_GOOD_BLOCK) {
      return result.block;
    } else {
      return NULL;
    }
  }
  return NULL;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while(try_merge_with_next(header)) { }
}
