#include "mem.h"
#include "mem_internals.h"
#include <assert.h>
#include <stdio.h>

size_t tests_passed = 0;

static struct block_header *block_get_header(void *contents) {
  return (struct block_header*) (((uint8_t*) contents) - offsetof(struct block_header, contents));
}

void test(bool test) {
  if (!test) {
    printf("FAILED\n\n");
  } else {
    tests_passed++;
    printf("PASSED\n\n");
  }
}

void print_test(int test_num, char* test_name) {
  printf("Test №%d: %s\n", test_num, test_name);
}

void* start_test() {
  void* addr = heap_init(REGION_MIN_SIZE);
  return addr;
}

void end_test() {
  heap_term();
}

bool malloc_test() {
  print_test(1, "malloc_test");
  void* heap = start_test();
  if (!heap) {
    end_test();
    return false;
  }
  end_test();
  return true;
}

bool block_free() {
  print_test(2, "block_free");
  void* heap = start_test();
  if (!heap) {
    end_test();
    return false;
  }

  void *ptr1 = _malloc(1);
  struct block_header *block = block_get_header(ptr1);

  if (block->is_free) {
    end_test();
    return false;
  }

  _free(ptr1);
  if (!block->is_free) {
    end_test();
    return false;
  }

  void *ptr2 = _malloc(2);
  if (ptr1 != ptr2) {
    end_test();
    return false;
  }

  end_test();
  return true;
}

bool two_block_free() {
  print_test(3, "two_block_free");
  void* heap = start_test();
  if (!heap) {
    end_test();
    return false;
  }

  void *ptr1 = _malloc(10);
  void *ptr2 = _malloc(10);
  struct block_header *block1 = block_get_header(ptr1);
  struct block_header *block2 = block_get_header(ptr2);

  if (block1->is_free || block2->is_free) {
    end_test();
    return false;
  }

  _free(ptr1);
  if (!block1->is_free || block2->is_free) {
    end_test();
    return false;
  }

  _free(ptr2);
  if (!block1->is_free || !block2->is_free) {
    end_test();
    return false;
  }

  end_test();
  return true;
}

bool extend_mem() {
  print_test(4, "extend_mem");
  void* heap = start_test();
  if (!heap) {
    end_test();
    return false;
  }

  void *ptr = _malloc(REGION_MIN_SIZE + 10);
  if (HEAP_START + offsetof(struct block_header, contents) != ptr) {
    end_test();
    return false;
  }
  
  end_test();
  return true;
}

bool extend_mem_another_place() {
  print_test(5, "extend_mem_another_place");
  void* heap = start_test();
  if (!heap) {
    end_test();
    return false;
  }

  void* data = mmap(heap + REGION_MIN_SIZE, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);

  _malloc(REGION_MIN_SIZE + 2);
  if (!data) {
    end_test();
    return false;
  }
  
  end_test();
  return true;
}

int main() {
  test(malloc_test());
  test(block_free());
  test(two_block_free());
  test(extend_mem());
  test(extend_mem_another_place());
  printf("RESULT: %zu/5 tests passed\n", tests_passed);
  return 0;
}
